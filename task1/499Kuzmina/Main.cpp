#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "PerlinNoise.h"

#include <iostream>
#include <vector>

class TerrianApplication : public Application {
public:


	MeshPtr _terrian;
	ShaderProgramPtr _shader;
	
	
	//Координаты источника света
    float _lr = 5.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;
    //Параметры источника света
    glm::vec3 _lightAmbientColor;
    glm::vec3 _lightDiffuseColor;
    
    //Параметры поверхности
    float _size = 3;
    unsigned int _frequency = 500;
    int _hillsDensity = 2;
    float _persistence = 3;
        
	
	void makeScene() override {

		Application::makeScene()
;
		_cameraMover = std::make_shared<FreeCameraMover>();

		_terrian = makeTerrian(_size, _frequency, _hillsDensity, _persistence);
		_terrian->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

		_shader = std::make_shared<ShaderProgram>("499KuzminaData/shader.vert", "499KuzminaData/shader.frag");	
		
		//=========================================================
		//Инициализация значений переменных освщения
		_lightAmbientColor = glm::vec3(0.2, 0.2, 0.2);
		_lightDiffuseColor = glm::vec3(0.8, 0.8, 0.8);
	}

	void draw() override {
		//~ Application::draw();
		
		//Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);
		
		glViewport(0, 0, width, height);
		
		//Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		//Подключаем шейдер	
		_shader->use();
		
		//Загружаем на видеокарту значения юниформ-переменных
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
		//~ _shader->setMat4Uniform("modelMatrix", _terrian->modelMatrix());
		
		glm::vec3 lightPos = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _shader->setVec3Uniform("light.pos", lightPos);
        _shader->setVec3Uniform("light.La", _lightAmbientColor);
        _shader->setVec3Uniform("light.Ld", _lightDiffuseColor);


		{
            _shader->setMat4Uniform("modelMatrix", _terrian->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _terrian->modelMatrix()))));

            _shader->setVec3Uniform("material.Ka", glm::vec3(0.0, 1.0, 0.0));
            _shader->setVec3Uniform("material.Kd", glm::vec3(0.0, 1.0, 0.0));

            _terrian->draw();
        }

		//~ _terrian->draw();
	}

};

int main() {
	TerrianApplication app;
	app.start();
	
	return 0;
}
