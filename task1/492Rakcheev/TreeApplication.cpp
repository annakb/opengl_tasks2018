#include "TreeApplication.h"

void TreeApplication::setupLSystem(
    std::map<char, std::string> rules,
    std::string initialString,
    float rotateAngle,
    float stepDistance,
    float distanceScale,
    float angleScale,
    float radiusScale,
    float initialBottomRadius,
    uint16_t numIterations
) {
    system = LSystem(rules, initialString, rotateAngle, stepDistance, distanceScale, angleScale, radiusScale, initialBottomRadius);
    system.executeIterations(numIterations);
    points = system.getPoints();
}


MeshPtr TreeApplication::makeBranch(glm::vec3 firstPoint, glm::vec3 secondPoint, float bottomRadius, float topRadius) {
    float distance = glm::distance(firstPoint, secondPoint);
    if (firstPoint[2] > secondPoint[2]) {
        glm::vec3 tmp = secondPoint;
        secondPoint = firstPoint;
        firstPoint = tmp;
        float tmpRadius = topRadius;
        topRadius = bottomRadius;
        bottomRadius = tmpRadius;
    }
    glm::vec3 projection(secondPoint[0], secondPoint[1], firstPoint[2]);
    glm::mat4 modelMatrix;
    glm::mat4 translation = glm::translate(glm::mat4(1.0f), firstPoint);
    if (projection != firstPoint) {
        float sin = glm::distance(projection, firstPoint) / distance;
        float angle = std::asin(sin);
        glm::vec3 axis = glm::cross(glm::vec3(0, 0, 1), secondPoint - firstPoint);
        modelMatrix = glm::rotate(translation, angle, axis);
    } else {
        modelMatrix = translation;
    }

    MeshPtr cone = makeCone(bottomRadius, topRadius, distance, firstPoint, secondPoint);
    cone->setModelMatrix(modelMatrix);
    branches.push_back(cone);
}


void TreeApplication::makeScene() {
    Application::makeScene();

    _cameraMover = std::make_shared<FreeCameraMover>();

    for (int i = 0; i < points.size(); i += 8) {
        makeBranch(
            glm::vec3(points[i], points[i + 1], points[i + 2]),
            glm::vec3(points[i + 4], points[i + 5], points[i + 6]),
            points[i + 3], points[i + 7]
        );
    }

    //Создаем шейдерную программу        
    shader = std::make_shared<ShaderProgram>("492RakcheevData/simple.vert", "492RakcheevData/simple.frag");
}

void TreeApplication::draw() {
    Application::draw();

    //Получаем размеры экрана (окна)
    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    //Устанавливаем порт вывода на весь экран (окно)
    glViewport(0, 0, width, height);

    //Очищаем порт вывода (буфер цвета и буфер глубины)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Подключаем шейдерную программу
    shader->use();

    //Загружаем на видеокарту значения юниформ-переменных
    shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    //Рисуем мешы
    for (auto branch: branches) {
        shader->setMat4Uniform("modelMatrix", branch->modelMatrix());
        branch->draw();
    }
}
