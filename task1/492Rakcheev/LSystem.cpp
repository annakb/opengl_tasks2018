#define E_MATH_DEFINES

#include "LSystem.h"

#include <math.h>
#include <iostream>
#include <stack>
#include <cstdlib>
#include <ctime>
#include <glm/gtc/matrix_transform.hpp>


LSystem::LSystem(
    const std::map<char, std::string>& rules,
    std::string initialString,
    float rotateAngle,
    float stepDistance,
    float distanceScale,
    float angleScale,
    float radiusScale,
    float initialBottomRadius
) :
    rules(rules),
    initialString(initialString),
    currentString(initialString),
    initialRotateAngle(rotateAngle * M_PI / 180),
    initialStepDistance(stepDistance),
    distanceScale(distanceScale),
    angleScale(angleScale),
    radiusScale(radiusScale),
    initialBottomRadius(initialBottomRadius) {
}


void LSystem::executeIterations(uint16_t numIterations) {
    for (uint16_t i = 0; i < numIterations; ++i) {
        std::string newString;
        for (auto ch: currentString) {
            auto rule = rules.find(ch);
            if (rule == rules.end()) {
                newString += ch;
            }
            else {
                newString += rule->second;
            }
        }
        currentString = newString;
    }
}


std::vector<float> LSystem::getPoints() {
    std::vector<float> result;

    State state;
    std::stack<State> states_stack;
    state.position = glm::vec4(0, 0, 0, 1);
    state.angles = glm::vec3(0, 0, 0);
    state.rotateAngle = initialRotateAngle;
    state.stepDistance = initialStepDistance;
    state.bottomRadius = initialBottomRadius;
    state.invert = 1;

    for (auto ch: currentString) {
        switch (ch) {
            case '[':
                states_stack.push(state);
                break;
            case ']':
                state = states_stack.top();
                states_stack.pop();
                break;
            case 'F':
                pushPosition(state, result);
                UpdateState(state);
                pushPosition(state, result);
                break;
            case 'f':
                UpdateState(state);
                break;
            case '!':
                state.invert *= -1;
                break;
            case '+':
                state.angles.z += state.invert*state.rotateAngle;
                break;
            case '-':
                state.angles.z -= state.invert*state.rotateAngle;
                break;
            case '&':
                state.angles.x += state.invert*state.rotateAngle;
                break;
            case '^':
                state.angles.x -= state.invert*state.rotateAngle;
                break;
            case '<':
                state.angles.y += state.invert*state.rotateAngle;
                break;
            case '>':
                state.angles.y -= state.invert*state.rotateAngle;
                break;
        }
    }
    return result;
}


glm::mat4 LSystem::getStep(const State& state) {
    glm::mat4 RotateX = glm::rotate(glm::mat4(1), state.angles.x, glm::vec3(1, 0, 0));
    glm::mat4 RotateYX = glm::rotate(RotateX, state.angles.y, glm::vec3(0, 1, 0));
    glm::mat4 RotateZYX= glm::rotate(RotateYX, state.angles.z, glm::vec3(0, 0, 1));
    glm::vec4 dim4_translate = RotateZYX * glm::vec4(0, state.stepDistance, 0, 0);
    glm::vec3 translate(dim4_translate.x, dim4_translate.y, dim4_translate.z);
    glm::mat4 StepTransformation = glm::translate(glm::mat4(1), translate);
    return StepTransformation;
}


void LSystem::pushPosition(const State& state, std::vector<float>& result) {
    result.push_back(state.position.x);
    result.push_back(state.position.z);
    result.push_back(state.position.y);
    result.push_back(state.bottomRadius);
}


void LSystem::UpdateState(State& state) {
    glm::mat4 StepTransformation = getStep(state);
    state.position = StepTransformation * state.position;
    state.stepDistance *= distanceScale;
    state.rotateAngle *= angleScale;
    state.bottomRadius *= radiusScale;
}
