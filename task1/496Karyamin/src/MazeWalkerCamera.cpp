#include <glm/vec3.hpp>
#include <utility>
#include "../include/MazeWalkerCamera.h"

//}


////
//// Created by avk on 22.03.18.
////
//
//#include "../include/MazeWalkerCamera.h"
//#include <glm/gtx/transform.hpp>
//#include <glm/gtx/quaternion.hpp>
//
//
//MazeWalkerCamera::MazeWalkerCamera() :
//    UP(0.0f, 0.0f, 1.0f),
//    _viewDirection(0.0f, 0.0f, -1.0f)
//{}
//
//glm::mat4 MazeWalkerCamera::getWorldToViewMatrix() const {
//    return glm::lookAt(_position, _position + _viewDirection, UP);
//}
//
//void MazeWalkerCamera::handleMouseMove(GLFWwindow* window, double x, double y) {
//
//    glm::vec2 newMousePosition(x,y);
//    glm::vec2 mouseDelta = newMousePosition - _oldMousePosition;
//
//    _viewDirection = glm::mat3(glm::rotate(mouseDelta.x, UP)) * _viewDirection;
//
//
//    _oldMousePosition = newMousePosition;
//}
//
//void MazeWalkerCamera::update(GLFWwindow *window, double dt) {
//    float speed = 1.0f;
//
//    //Получаем текущее направление "вперед" в мировой системе координат
//    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rotation;
//
//    //Получаем текущее направление "вправо" в мировой системе координат
//    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rotation;
//
//    //Двигаем камеру вперед/назад
//    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
//    {
//        _position += forwDir * speed * static_cast<float>(dt);
//    }
//    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
//    {
//        _position -= forwDir * speed * static_cast<float>(dt);
//    }
//    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
//    {
//        _position -= rightDir * speed * static_cast<float>(dt);
//    }
//    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
//    {
//        _position += rightDir * speed * static_cast<float>(dt);
//    }
//
//    //-----------------------------------------
//
//    //Соединяем перемещение и поворот вместе
//    _camera.viewMatrix = glm::toMat4(-_rotation) * glm::translate(-_position);
//
//    //-----------------------------------------
//
//    int width, height;
//    glfwGetFramebufferSize(window, &width, &height);
//
//    //Обновляем матрицу проекции на случай, если размеры окна изменились
//    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 100.f);
//


void MazeWalkerCamera::update(GLFWwindow *window, double dt) {
    glm::vec3 newPos = calcNewUpdatePos(window, dt);

    DontMoveAxis r;
    r |= _checkFloorCollision_(newPos);
    r |= _checkCeilCollision_(newPos);
    r |= _checkWallsCollision_(newPos);

    r.updatePos(_pos, newPos);
    _updateMatrices_(window, _pos);

}

MazeWalkerCamera::MazeWalkerCamera(MazePtr maze) : _maze(std::move(maze)) { }

MazeWalkerCamera::DontMoveAxis MazeWalkerCamera::_checkCollision_(const glm::vec3 &cameraPos) const {
    return _checkCeilCollision_(cameraPos) ||
           _checkFloorCollision_(cameraPos) ||
           _checkWallsCollision_(cameraPos);
}

MazeWalkerCamera::DontMoveAxis MazeWalkerCamera::_checkWallsCollision_(const glm::vec3 &cameraPos) const {
    DontMoveAxis result;
    for (const auto& wall : _maze->getWalls()) {
        result |= _checkOneWallCollision(cameraPos, wall);
    }

    return result;
}

MazeWalkerCamera::DontMoveAxis MazeWalkerCamera::_checkOneWallCollision(const glm::vec3 &cameraPos, const Wall& wall) const {
    DontMoveAxis r;

    Point a = wall.px;
    Point b = wall.py;

    if (a.x == b.x) {
        r.X = std::abs(cameraPos.x - a.x) < COLLISION_DIST
                && between(cameraPos.y, a.y, b.y);
    } else if (a.y == b.y) {
        r.Y = std::abs(cameraPos.y - a.y) < COLLISION_DIST
                && between(cameraPos.x, a.x, b.x);
    }

    return r;
}

MazeWalkerCamera::DontMoveAxis MazeWalkerCamera::_checkFloorCollision_(const glm::vec3 &cameraPos) const {
    DontMoveAxis r;
    r.Z = std::abs(cameraPos.z) < COLLISION_DIST
           && cameraPos.x > 0 && cameraPos.x < 3
           && cameraPos.y > 0 && cameraPos.y < 3;
    return r;
}

MazeWalkerCamera::DontMoveAxis MazeWalkerCamera::_checkCeilCollision_(const glm::vec3 &cameraPos) const {
    DontMoveAxis r;
    r.Z = std::abs(cameraPos.z - 1.0f) < COLLISION_DIST
           && cameraPos.x > 0 && cameraPos.x < 3
           && cameraPos.y > 0 && cameraPos.y < 3;
    return r;
}
