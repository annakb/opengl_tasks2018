#define _USE_MATH_DEFINES

#include "LSystem.h"

#include <math.h>
#include <iostream>
#include <stack>
#include <glm/gtc/matrix_transform.hpp>


LSystem::LSystem(
    const std::map<char, std::string>& rules,
    std::string initialString,
    float stepDistance,
    float rotateAngle,
    float thickness,
    float distanceScale,
    float angleScale,
    float thicknessScale
) :
    rules(rules),
    initialString(initialString),
    currentString(initialString),
    initialRotateAngle(rotateAngle * M_PI / 180),
    initialStepDistance(stepDistance),
    initialThickness(thickness),
    distanceScale(distanceScale),
    angleScale(angleScale),
    thicknessScale(thicknessScale) {
}


void LSystem::executeIterations(uint16_t numIterations) {
    for (uint16_t i = 0; i < numIterations; ++i) {
        std::string newString;
        for (auto ch: currentString) {
            auto rule = rules.find(ch);
            if (rule == rules.end())
                newString += ch;
            else
                newString += rule->second;
        }
        currentString = newString;
    }
}


std::vector<ConeParams> LSystem::getConesParams() {
    std::vector<ConeParams> result;

    State state;
    std::stack<State> states_stack;
    state.position = glm::vec4(0, -0.8, 0, 1);
    state.angles = glm::vec3(0, 0, 0);
    state.rotateAngle = initialRotateAngle;
    state.stepDistance = initialStepDistance;
    state.thickness = initialThickness;
    state.invert = 1;

    ConeParams params;
    params.thicknessScale = thicknessScale;
    for (auto ch: currentString) {
        switch (ch) {
            case '[':
                states_stack.push(state);
                break;
            case ']':
                state = states_stack.top();
                states_stack.pop();
                break;
            case 'F':
                params.firstPoint = getPoint(state);
                params.thickness = state.thickness;
                UpdateState(state);
                params.secondPoint = getPoint(state);
                result.push_back(params);
                break;
            case 'f':
                UpdateState(state);
                break;
            case '!':
                state.invert *= -1;
                break;
            case '+':
                state.angles.z += state.invert*state.rotateAngle;
                break;
            case '-':
                state.angles.z -= state.invert*state.rotateAngle;
                break;
            case '&':
                state.angles.x += state.invert*state.rotateAngle;
                break;
            case '^':
                state.angles.x -= state.invert*state.rotateAngle;
                break;
            case '<':
                state.angles.y += state.invert*state.rotateAngle;
                break;
            case '>':
                state.angles.y -= state.invert*state.rotateAngle;
                break;
        }
    }
    return result;
}


glm::mat4 LSystem::getStep(const State& state) {
    glm::mat4 RotateX = glm::rotate(glm::mat4(1), state.angles.x, glm::vec3(1, 0, 0));
    glm::mat4 RotateYX = glm::rotate(RotateX, state.angles.y, glm::vec3(0, 1, 0));
    glm::mat4 RotateZYX = glm::rotate(RotateYX, state.angles.z, glm::vec3(0, 0, 1));
    glm::vec4 dim4_translate = RotateZYX * glm::vec4(0, state.stepDistance, 0, 0);
    glm::vec3 translate(dim4_translate.x, dim4_translate.y, dim4_translate.z);
    glm::mat4 StepTransformation = glm::translate(glm::mat4(1), translate);
    return StepTransformation;
}


glm::vec3 LSystem::getPoint(const State& state) {
    return glm::vec3(state.position.x, state.position.y, state.position.z);
}


void LSystem::UpdateState(State& state) {
    glm::mat4 StepTransformation = getStep(state);
    state.position = StepTransformation * state.position;
    state.stepDistance *= distanceScale;
    state.rotateAngle *= angleScale;
    state.thickness *= thicknessScale;
}
