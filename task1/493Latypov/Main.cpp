#include "SurfaceApplication.hpp"

int main() {
    SurfaceApplication app;
    app.start();
    return 0;
}