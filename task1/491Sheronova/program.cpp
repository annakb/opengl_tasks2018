#include <string>

#define GLEW_STATIC
#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include "Shader.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "SOIL.h"


#define COUNTX 450
#define COUNTZ 450
#define VERTEX_COUNT (COUNTX*COUNTZ)
#define SIZE 1.0f
#define SCALE 1000

GLuint screenWidth = 800, screenHeight = 600;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void Do_Movement();
void mouse_callback(GLFWwindow* window, double xpos, double ypos);

glm::vec3 cameraPos   = glm::vec3(0.0f, 0.2f,  1.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.2f, 10.0f);
glm::vec3 cameraUp    = glm::vec3(0.0f, 1.0f,  0.0f);
GLfloat yAngle   = -90.0f;	
GLfloat xAngle =   0.0f;
GLfloat X =  screenWidth  / 2.0;
GLfloat Y =  screenHeight / 2.0;
GLfloat curHeight = 0;

bool keys[1024];

int main()
{
    	glfwInit();
    	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    	glfwWindowHint(GLFW_SAMPLES, 4);

    	GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "Relief", nullptr, nullptr); 
	glfwMakeContextCurrent(window);

	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);

        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glewExperimental = GL_TRUE;
	glewInit();

	glViewport(0, 0, screenWidth, screenHeight);

	glEnable(GL_DEPTH_TEST);
	
	Shader ourShader("491SheronovaData/shaders/shader.vert", "491SheronovaData/shaders/shader.frag");

	

	GLfloat vertices[VERTEX_COUNT * 3];
	float posX, posZ;
	posZ = -SIZE / 2;
	for (int z = 0; z < COUNTZ; z++)
	{
		posX = -SIZE / 2;
		for (int x = 0; x < COUNTX; x++)
		{
			vertices[3 * x + z * COUNTX * 3] = posX;
			vertices[3 * x + z * COUNTX * 3 + 1] = 0;
			vertices[3 * x + z * COUNTX * 3 + 2] = posZ;
			posX += (SIZE / (COUNTX - 1));
		}
		posZ += (SIZE / (COUNTX - 1));
	}
	
	GLuint indices[(COUNTX - 1) * (COUNTZ - 1) * 2 * 3];
	int j = 0;
	for (int x = 0; x < COUNTX - 1; ++x)
	{	
		for (int z = 0; z < COUNTZ - 1; ++z)
		{
			indices[j++] = x + z * COUNTX;
			indices[j++] = x + (z + 1) * COUNTX;
			indices[j++] = (x + 1) + z * COUNTX;
			indices[j++] = x + (z + 1) * COUNTX;
			indices[j++] = (x + 1) + (z + 1) * COUNTX;
			indices[j++] = (x + 1) + z * COUNTX;
		}
	}    
	
	GLuint VBO, VAO, EBO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);

    	glBindBuffer(GL_ARRAY_BUFFER, VBO);
    	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	
	// Position atribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
    	glEnableVertexAttribArray(0);
	
	glBindVertexArray(0);

	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	int width, height;
	unsigned char* image = SOIL_load_image("491SheronovaData/height.jpg", &width, &height, 0, SOIL_LOAD_RGB);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(image);
	glBindTexture(GL_TEXTURE_2D, 0); 
	
	
	while(!glfwWindowShouldClose(window))
    	{
        	glfwPollEvents();
		Do_Movement();

        	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
        	ourShader.Use();
		
		GLint sizeLoc = glGetUniformLocation(ourShader.Program, "size");
        	glUniform1f(sizeLoc, SIZE); 
        	glm::mat4 model;
		model = glm::rotate(model, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
          	model = glm::scale(model, glm::vec3(SCALE));   
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glm::mat4 view;
        	view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
        	glm::mat4 projection;	
        	projection = glm::perspective(45.0f, (float)screenWidth/(float)screenHeight, 0.1f, 1000.0f);
        	GLint modelLoc = glGetUniformLocation(ourShader.Program, "model");
        	GLint viewLoc = glGetUniformLocation(ourShader.Program, "view");
        	GLint projLoc = glGetUniformLocation(ourShader.Program, "projection");
        	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		GLint colorLoc = glGetUniformLocation(ourShader.Program, "lineColor");
		glUniform4f(colorLoc, 0.0f, 0.0f, 0.0f, 1.0f);
        	glBindVertexArray(VAO);
       		glDrawElements(GL_TRIANGLES, (COUNTX - 1) * (COUNTZ - 1) * 2 * 3, GL_UNSIGNED_INT, 0); 
		glUniform4f(colorLoc, 1.0f, 0.0f, 0.0f, 1.0f);
		glEnable(GL_POLYGON_OFFSET_LINE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glPolygonOffset(0.0f, 0.0f);
		glDrawElements(GL_TRIANGLES, (COUNTX - 1) * (COUNTZ - 1) * 2 * 3, GL_UNSIGNED_INT, 0);
		glDisable(GL_POLYGON_OFFSET_LINE);
		glBindVertexArray(0);
        	glfwSwapBuffers(window);
    	}
    	glDeleteVertexArrays(1, &VAO);
    	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);
    	glfwTerminate();
    	return 0;
}

void Do_Movement()
{ 
	GLfloat cameraSpeed = 1.0f;
    	if (keys[GLFW_KEY_W])
        	cameraPos += cameraSpeed * glm::normalize(cameraFront);
    	if (keys[GLFW_KEY_S])
        	cameraPos -= cameraSpeed * glm::normalize(cameraFront);
    	if (keys[GLFW_KEY_A])
        	cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    	if (keys[GLFW_KEY_D])
        	cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
}


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
   	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        	glfwSetWindowShouldClose(window, GL_TRUE);
    	if (key >= 0 && key < 1024)
    	{
        	if (action == GLFW_PRESS)
            		keys[key] = true;
        	else if (action == GLFW_RELEASE)
            		keys[key] = false;
    	}
}


void mouse_callback(GLFWwindow* window, double newX, double newY)
{
    xAngle += (newX - X) * 0.1f;
    yAngle += (Y - newY) * 0.1f;
    X = newX;
    Y = newY;

    if(yAngle > 89.9f)
        yAngle = 89.9f;
    if(yAngle < -89.9f)
        yAngle = -89.9f;

    glm::vec3 front;
    front.x = cos(glm::radians(xAngle)) * cos(glm::radians(yAngle));
    front.y = sin(glm::radians(yAngle));
    front.z = sin(glm::radians(xAngle)) * cos(glm::radians(yAngle));
    cameraFront = glm::normalize(front);
}
