#pragma once

#define GLM_FORCE_RADIANS
#include <Mesh.hpp>
#include <math.h>
#include <vector>
#include <iostream>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/glm.hpp>
#include <GL/glew.h>


MeshPtr makeCylinder(float radius, float radiusScale, float height, int numPolygons, bool rotateFlag);
